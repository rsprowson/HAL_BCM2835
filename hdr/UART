;
; Copyright (c) 2012, RISC OS Open Ltd
; Copyright (c) 2012, Adrian Lees
; All rights reserved.
;
; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:
;     * Redistributions of source code must retain the above copyright
;       notice, this list of conditions and the following disclaimer.
;     * Redistributions in binary form must reproduce the above copyright
;       notice, this list of conditions and the following disclaimer in the
;       documentation and/or other materials provided with the distribution.
;     * Neither the name of RISC OS Open Ltd nor the names of its contributors
;       may be used to endorse or promote products derived from this software
;       without specific prior written permission.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
; POSSIBILITY OF SUCH DAMAGE.
;
; With many thanks to Broadcom Europe Ltd for releasing the source code to
; its Linux drivers, thus making this port possible.
;

; UART clock
UARTCLK * 3000000

; UARTCR bits
CR_CTSEN  * 1:SHL:16 ; Hardware CTS
CR_RTSEN  * 1:SHL:15 ; Hardware RTS
CR_RTS    * 1:SHL:11 ; RTS
CR_RXE    * 1:SHL:9  ; RX enable
CR_TXE    * 1:SHL:8  ; TX enable
CR_LBE    * 1:SHL:7  ; Loopback
CR_UARTEN * 1:SHL:0  ; UART enable

; UARTLCRH bits
LCRH_SPS  * 1:SHL:7 ; Sticky parity
LCRH_WLEN * 3:SHL:5 ; Word length
LCRH_WLEN_shift * 5
LCRH_FEN  * 1:SHL:4 ; FIFO enable
LCRH_STP2 * 1:SHL:3 ; 2 stop bits
LCRH_EPS  * 1:SHL:2 ; Even parity
LCRH_PEN  * 1:SHL:1 ; Parity enable
LCRH_BRK  * 1:SHL:0 ; Break enable

; UARTFLAG bit assignments
FLAG_TXFE * 7 ; TX FIFO empty
FLAG_RXFF * 6 ; RX FIFO full
FLAG_TXFF * 5 ; TX FIFO full
FLAG_RXFE * 4 ; RX FIFO empty
FLAG_BUSY * 3 ; UART busy transmitting
FLAG_CTS  * 0

; UARTDR bit assignments
DR_OE * 11 ; Overrun error
DR_BE * 10 ; Break error
DR_PE * 9  ; Parity error
DR_FE * 8  ; Framing error

; UARTRSRECR bit assignments
RSR_OE * 3 ; Overrun error
RSR_BE * 2 ; Break error
RSR_PE * 1 ; Parity error
RSR_FE * 0 ; Framing error

; UARTIMSC, UARTRIS, UARTMIS, UARTICR bit assignments
UI_OE * 10 ; Overrun error
UI_BE * 9  ; Break error
UI_PE * 8  ; Parity error
UI_FE * 7  ; Framing error
UI_RT * 6  ; RX timeout
UI_TX * 5  ; TX FIFO empty threshold crossed
UI_RX * 4  ; RX FIFO full threshold crossed
UI_CTS * 1 ; CTS

                END
