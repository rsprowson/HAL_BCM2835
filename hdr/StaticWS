;
; Copyright (c) 2012, RISC OS Open Ltd
; Copyright (c) 2012, Adrian Lees
; All rights reserved.
;
; Redistribution and use in source and binary forms, with or without
; modification, are permitted provided that the following conditions are met:
;     * Redistributions of source code must retain the above copyright
;       notice, this list of conditions and the following disclaimer.
;     * Redistributions in binary form must reproduce the above copyright
;       notice, this list of conditions and the following disclaimer in the
;       documentation and/or other materials provided with the distribution.
;     * Neither the name of RISC OS Open Ltd nor the names of its contributors
;       may be used to endorse or promote products derived from this software
;       without specific prior written permission.
;
; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
; IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
; ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
; LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
; CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
; SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
; CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
; POSSIBILITY OF SUCH DAMAGE.
;
; With many thanks to Broadcom Europe Ltd for releasing the source code to
; its Linux drivers, thus making this port possible.
;

                GET     Hdr:OSEntries
                GET     Hdr:HALDevice
                GET     Hdr:SDHCIDevice
                GET     Hdr:DMADevice
                GET     Hdr:GPIODevice
                GET     Hdr:VideoDevice
                GET     Hdr:DBellDevice
                GET     hdr.BCM2835

; Per-timer workspace layout
                            ^             0
Timer_Reload                #             4       ; interval between interrupts
Timer_Compare               #             4       ; soft copy of most recently programmed compare value
Timer_Register              #             4       ; address of compare register
Timer_Device                #             4       ; device number
TimerWsSize                 *             :INDEX: @

; Per-DMA channel workspace
                       ^ 0, a1
DMACDevice             # HALDevice_DMAL_Size
DMACWorkspace          # 4 ; HAL workspace ptr
DMACChanMask           # 4 ; DMA_ENABLE bit for this channel
DMACDREQ               # 4 ; Peripheral/DREQ this channel is servicing
DMACOptions            # 4 ; Options set by SetOptions
DMACPeriAddress        # 4 ; VC phys addr of peripheral - i.e. at &7e......
DMACLastProgress       # 4 ; Last progress value
DMACLastCONBLK_AD      # 4 ; Last control block seen executing
DMACLastTXFR_LEN       # 4 ; Last transfer length remaining seen
DMACCBOffset           # 4 ; Address offset to convert DMA CB addr to ARM CB addr
DMACDesc               # 32 ; Buffer for description string
DMAC_DeviceSize        * :INDEX: @

; VCHIQ HAL device layout

                                 ^ 0
                                 # HALDeviceSize
HALDevice_VCHIQARMToVCOffset     # 4
HALDevice_VCHIQVCDoorbell        # 4 ; Doorbell to interrupt VC
HALDevice_VCHIQARMDoorbell       # 4 ; Doorbell to interrupt us
HALDevice_VCHIQInitVC            # 4
HALDevice_VCHIQ_Size             * :INDEX: @

; Device-specific struct for the VDU device

                        ^ 0
VDUDevSpec_SizeField    # 4 ; Size field
VDUDevSpec_DMAChan      # 4 ; Pointer to DMA channel
VDUDevSpec_BurstLen     # 4 ; Burst length for use with DMA channel
VDUDevSpec_Size         # 0 ; Size value to write to size field

; Main workspace layout

sb              RN      9

                ^       0,sb

PeriBase        #       4
IRQ_Base_Address #      4
QA7_Base        #       4
ARM_Counter_IO_Address # 4
ARM_Timer_IO_Address #  4
UARTOldModemStatus #    4
MMUOffBaseAddr  #       4                    ; original address kernel was loaded from
MachineID       #       8                    ; derived from MAC address if there
Spinlock        #       4 ; Spinlock used for any atomic ops

IIC_Status      #       4 ; Non-zero if an IIC transfer is going on
; NOTE - The following locations are for the base addresses of the
; controllers that RISC OS uses for bus 0 and bus 1.  The use is
; dependent on board revision.  Revision 1 boards (Board_Revision
; values 0..3) use Broadcom controller 0 for RISC OS bus 0 and
; Broadcom 1 for RISC OS 1.  Revision 2 boards (Board_Revision
; values 4 upwards) use Broadcom controller 1 for RISC OS bus 0
; and Broadcom controller 0 for RISC OS bus 1, because the PCB
; layout exchanged the busses.
IIC_Base        #       4 ; Base address of IIC controller for RO bus 0
;               #       4 ; Base address of IIC controller for RO bus 1
; NOTE - Bus 1 isn't yet implemented!

Timer   SETA    0
        WHILE   Timer < NumTimers
Timer$Timer.Ws  #       TimerWsSize
Timer   SETA    Timer + 1
        WEND

FB_CacheMode    #       4

; info interrogated from the VC side
ARM_Base        #       4
ARM_Size        #       4
VC_Base         #       4
VC_Size         #       4
Board_Model     #       4
Board_Revision  #       4
ARM_DMAChannels #       4
VirtGPIOBuf     #       4
SafetyCatch     #       4 ; Only valid on Compute Module 3

; align to 16 byte boundary NB this isnt aligned once hal initialised
                #       (((:INDEX:@)+15):AND::NOT:15)-(:INDEX:@)
tagbuffer       #       300 ; platform query buffer

NCNBAddr        #       4       ;NCNB workspace
NCNBPhysAddr    #       4       ;VC physical address of NCNB workspace

OSheader        #       4
OSentries       #       4*(HighestOSEntry+1)

SimulatedCMOS   #       2048+4  ;Usual 2k plus version word (as appended by *SaveCMOS)

SDHCIWriteInterval #    4 ; minimum counter ticks between writes
SDHCILastWriteCount #   4 ; counter value at last write
SDHCIInputClock #       4 ; estimated speed of input clock to SDHCI block
                #       (16-:INDEX:@):AND:15         ; align nicely
SDHCIDevice     #       HALDevice_SDHCISize          ; see Hdr:SDHCIDevice
SDHCISlotInfo   #       HALDeviceSDHCI_SlotInfo_Size ; the controller has just the 1 slot

DMAFreeChannels #       4 ; Mask of which physical DMA channels are free
DMANumChannels  #       4 ; Count of how many channel devices exist
DMAChannelList  #       DMA_CH_Count*4 ; List of channel devices for Enumerate
                #       (16-:INDEX:@):AND:15         ; align nicely
DMAController   #       HALDevice_DMAC_Size_0_1      ; see Hdr:HALDevice
DMAChannels     #       DMAC_DeviceSize*DMA_CH_Count ; List of channel devices (indexed by physical channel #)

VCHIQDevice     #       HALDevice_VCHIQ_Size
GPIO0Device     #       HALDevice_GPIO_Size_1_0 + 16
GPIO1Device     #       HALDevice_GPIO_Size_1_0 + 16
VDUDevice       #       HALDevice_VDU_Size
VDUDevSpec      #       VDUDevSpec_Size
RTCDeviceStruct #       80
SPI0Device      #       HALDeviceSize
SPI1Device      #       HALDeviceSize
SPI2Device      #       HALDeviceSize
TouchDevice     #       HALDeviceSize
MBoxDevice      #       HALDeviceSize
DBellDevice     #       HALDevice_DBell_Size

HAL_WsSize              *       :INDEX:@
sizeof_workspace        *       :INDEX:@

                END
